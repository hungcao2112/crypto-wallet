//
//  AppDelegate.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 15/06/2021.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //Override system font
        UIFont.overrideInitialize()
        
        //Initial viewcontroller
        let walletVC = WalletRouter.createModule()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = walletVC
        window?.makeKeyAndVisible()
        
        return true
    }
}

