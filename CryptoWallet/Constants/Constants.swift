//
//  Constants.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 16/06/2021.
//

import Foundation
import UIKit

struct App {
    
    let delegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    struct Config {
        static let apiBaseURL = "https://www.coinhako.com/api"
        static let apiVersion = 3
    }
}
