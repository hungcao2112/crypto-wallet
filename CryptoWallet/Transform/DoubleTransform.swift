//
//  DoubleTransform.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 17/06/2021.
//

import Foundation
import ObjectMapper

class DoubleTransform: TransformType {
    
    typealias Object = Double
    
    typealias JSON = String
    
    public init() {}
    
    func transformFromJSON(_ value: Any?) -> Double? {
        guard let string = value as? String else {
            return nil
        }
        return Double(string)
    }
    
    func transformToJSON(_ value: Double?) -> String? {
        guard let data = value else {
            return nil
        }
        return "\(data)"
    }
}
