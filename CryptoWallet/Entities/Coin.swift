//
//  Coin.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 16/06/2021.
//

import Foundation
import ObjectMapper

class Coin: Mappable {
    
    var base: String?
    var counter: String?
    var buyPrice: String?
    var sellPrice: String?
    var icon: String?
    var name: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        base <- map["base"]
        counter <- map["counter"]
        buyPrice <- map["buy_price"]
        sellPrice <- map["sell_price"]
        icon <- map["icon"]
        name <- map["name"]
    }
}
