//
//  WalletRouter.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 18/06/2021.
//

import Foundation
import UIKit

class WalletRouter: WalletPresenterToRouterProtocol {
    
    // MARK: - Methods
    static func createModule() -> UIViewController {
        let view = R.storyboard.wallet.walletViewController()!
        let presenter = WalletPresenter()
        let interactor = WalletInteractor()
        let router = WalletRouter()
        
        view.presenter = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        let navigationView = UINavigationController(rootViewController: view)
        
        return navigationView
    }
}
