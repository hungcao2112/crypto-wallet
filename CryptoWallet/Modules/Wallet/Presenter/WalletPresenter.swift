//
//  WalletPresenter.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 17/06/2021.
//

import Foundation

class WalletPresenter: WalletViewToPresenterProtocol {
    
    // MARK: - Properties
    var view: WalletPresenterToViewProtocol?
    var interactor: WalletPresenterToInteractorProtocol?
    var router: WalletPresenterToRouterProtocol?
    
    // MARK: - Methods
    func updateView() {
        view?.showProgressLoading()
        interactor?.fetchCoinPrices()
    }
    
    func getCoinPricesCount() -> Int {
        return interactor?.coinPrices.count ?? 0
    }
    
    func getCoinPrice(index: Int) -> Coin? {
        return interactor?.coinPrices[index]
    }
    
    func didTapMoreButton(index: Int) {
        guard let selectedFilterType = interactor?.selectedFilterType else {
            return
        }
        switch selectedFilterType {
        case .all:
            view?.showAddToFavoritesActionSheet(index: index)
        case .favorite:
            view?.showRemoveFromFavoritesActionSheet(index: index)
        }
    }
    
    func didTapAddToFavorites(index: Int) {
        interactor?.addCoinToFavorites(index: index)
    }
    
    func didTapRemoveFromFavorites(index: Int) {
        interactor?.removeCoinFromFavorites(index: index)
    }
    
    func didTapShowAllPrices() {
        interactor?.selectedFilterType = .all
        interactor?.getAllCoinPrices()
    }
    
    func didTapShowFavoritePrices() {
        interactor?.selectedFilterType = .favorite
        interactor?.getFavoriteCoinPrices()
    }
    
    func searchBarTextDidChanged(_ searchText: String) {
        interactor?.searchCoinPrices(searchText: searchText)
    }
    
    func searchBarTextDidCleared() {
        interactor?.clearSearchCoinPrices()
    }
}

// MARK: - WalletInteractorToPresenterProtocol
extension WalletPresenter: WalletInteractorToPresenterProtocol {
    func coinPricesFetched() {
        view?.hideProgressLoading()
        view?.showCoinPrices()
    }
    
    func coinPricesFetchFailed(_ error: ApiError) {
        view?.hideProgressLoading()
        view?.showError(error)
    }
}
