//
//  WalletProtocols.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 18/06/2021.
//

import Foundation
import UIKit

protocol WalletPresenterToViewProtocol: AnyObject {
    func showCoinPrices()
    func showError(_ error: ApiError)
    func showProgressLoading()
    func hideProgressLoading()
    func showAddToFavoritesActionSheet(index: Int)
    func showRemoveFromFavoritesActionSheet(index: Int)
}

protocol WalletInteractorToPresenterProtocol: AnyObject {
    func coinPricesFetched()
    func coinPricesFetchFailed(_ error: ApiError)
}

protocol WalletPresenterToInteractorProtocol: AnyObject {
    var presenter: WalletInteractorToPresenterProtocol? { get set }
    var selectedFilterType: FilterType { get set }
    var allCoinPrices: [Coin] { get }
    var coinPrices: [Coin] { get }
    var searchKey: String { get }
    
    func fetchCoinPrices()
    
    func getFavoriteCoinPrices()
    func getAllCoinPrices()
    
    func addCoinToFavorites(index: Int)
    func removeCoinFromFavorites(index: Int)
    
    func searchCoinPrices(searchText: String)
    func clearSearchCoinPrices()
}

protocol WalletPresenterToRouterProtocol: AnyObject {
    static func createModule() -> UIViewController
}

protocol WalletViewToPresenterProtocol: AnyObject {
    var view: WalletPresenterToViewProtocol? { get set }
    var interactor: WalletPresenterToInteractorProtocol? { get set }
    var router: WalletPresenterToRouterProtocol? { get set }
    
    func updateView()
    func getCoinPricesCount() -> Int
    func getCoinPrice(index: Int) -> Coin?
    
    func didTapMoreButton(index: Int)
    func didTapAddToFavorites(index: Int)
    func didTapRemoveFromFavorites(index: Int)
    
    func didTapShowAllPrices()
    func didTapShowFavoritePrices()
    
    func searchBarTextDidChanged(_ searchText: String)
    func searchBarTextDidCleared()
}
