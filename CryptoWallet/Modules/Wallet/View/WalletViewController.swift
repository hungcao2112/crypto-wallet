//
//  WalletViewController.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 17/06/2021.
//

import UIKit
import SVProgressHUD

class WalletViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Properties
    var presenter: WalletViewToPresenterProtocol?
    
    var searchController = UISearchController(searchResultsController: nil)
    
    // MARK: - Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupSearchController()
        setupTableView()
        
        presenter?.updateView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableViewHeaderSizeToFit()
    }
    
    private func setupNavigationBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.barTintColor = .white
        navigationController?.navigationBar.tintColor = .systemBlue
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black, .font: UIFont.boldSystemFont(ofSize: 18)]
        navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.black, .font: UIFont.boldSystemFont(ofSize: 36)]
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()

        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        let rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "arrow.counterclockwise"), style: .plain, target: self, action: #selector(didTapRightBarButtonItem))
        navigationItem.rightBarButtonItem = rightBarButtonItem
    }
    
    private func setupTableView() {
        tableView.register(UINib(resource: R.nib.coinTableViewCell), forCellReuseIdentifier: R.nib.coinTableViewCell.identifier)
        tableView.tableFooterView = UIView()
        
        let headerView = CoinTableHeaderView()
        
        headerView.allPricesButtonTapped = { [weak self] in
            self?.presenter?.didTapShowAllPrices()
        }
        
        headerView.favoriteButtonTapped = { [weak self] in
            self?.presenter?.didTapShowFavoritePrices()
        }
        
        tableView.tableHeaderView = headerView
    }
    
    private func setupSearchController() {
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.textField?.font = UIFont.systemFont(ofSize: 16)
        
        searchController.searchBar.delegate = self
    }
    
    private func tableViewHeaderSizeToFit() {
        let headerView = tableView.tableHeaderView!

        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()

        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame

        tableView.tableHeaderView = headerView
    }
    
    @objc private func didTapRightBarButtonItem() {
        presenter?.updateView()
    }
}

// MARK: - UITableViewDataSource & UITableViewDelegate
extension WalletViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.getCoinPricesCount() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.coinTableViewCell, for: indexPath)!
        let coin = presenter?.getCoinPrice(index: indexPath.row)
        cell.configure(coin: coin)
        cell.moreButtonTapped = { [weak self] in
            self?.presenter?.didTapMoreButton(index: indexPath.row)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

// MARK: - WalletPresenterToViewProtocol
extension WalletViewController: WalletPresenterToViewProtocol {
    func showAddToFavoritesActionSheet(index: Int) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Add to favorites", style: UIAlertAction.Style.default) { _ in
            self.presenter?.didTapAddToFavorites(index: index)
        })
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func showRemoveFromFavoritesActionSheet(index: Int) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Remove from favorites", style: UIAlertAction.Style.destructive) { _ in
            self.presenter?.didTapRemoveFromFavorites(index: index)
        })
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func showProgressLoading() {
        self.showLoading()
    }
    
    func hideProgressLoading() {
        self.hideLoading()
    }
    
    func showCoinPrices() {
        tableView.reloadData()
    }
    
    func showError(_ error: ApiError) {
        let alert = UIAlertController(title: "Error", message: error.message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - UISearchBarDelegate
extension WalletViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter?.searchBarTextDidChanged(searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        presenter?.searchBarTextDidCleared()
    }
}
