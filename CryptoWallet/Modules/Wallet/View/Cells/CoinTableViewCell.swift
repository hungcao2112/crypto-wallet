//
//  CoinTableViewCell.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 17/06/2021.
//

import UIKit
import Kingfisher

class CoinTableViewCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var baseLabel: UILabel!
    @IBOutlet weak var buyPriceLabel: UILabel!
    @IBOutlet weak var sellPriceLabel: UILabel!
    
    var moreButtonTapped: (()->())?
    
    func configure(coin: Coin?) {
        iconImageView.kf.setImage(with: URL(string: coin?.icon ?? ""))
        nameLabel.text = coin?.name
        baseLabel.text = coin?.base
        sellPriceLabel.text = "\(coin?.sellPrice ?? "-") \(coin?.counter ?? "")"
        buyPriceLabel.text = "\(coin?.buyPrice ?? "-") \(coin?.counter ?? "")"
    }
    
    
    @IBAction func didTapMoreButton(_ sender: UIButton) {
        moreButtonTapped?()
    }
}
