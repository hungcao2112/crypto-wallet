//
//  CoinTableHeaderView.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 18/06/2021.
//

import UIKit

class CoinTableHeaderView: CustomView {

    @IBOutlet weak var allButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    
    var allPricesButtonTapped: (()->())?
    var favoriteButtonTapped: (()->())?
    
    override func getView() -> UIView? {
        return R.nib.coinTableHeaderView(owner: self)
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        setupUI()
    }

    private func setupUI() {
        [allButton, favoriteButton].forEach {
            $0?.setBackgroundColor(R.color.primary_light_gray()!, for: .normal)
            $0?.setBackgroundColor(.systemBlue.withAlphaComponent(0.1), for: .selected)
        }
        allButton.isSelected = true
    }
    
    @IBAction func didTapButton(_ sender: UIButton) {
        [allButton, favoriteButton].forEach {
            $0?.isSelected = false
        }
        sender.isSelected = true
        if sender == allButton {
            allPricesButtonTapped?()
        }
        else if sender == favoriteButton {
            favoriteButtonTapped?()
        }
        
    }
}
