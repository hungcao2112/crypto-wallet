//
//  WalletInteractor.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 17/06/2021.
//

import Foundation

enum FilterType {
    case all
    case favorite
}

class WalletInteractor: WalletPresenterToInteractorProtocol {
    
    var presenter: WalletInteractorToPresenterProtocol?
    
    var selectedFilterType: FilterType = .all
    var allCoinPrices: [Coin] = []
    var coinPrices: [Coin] = []
    var searchKey: String = ""
    
    private var timer: Timer?
    
    func fetchCoinPrices() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 30, repeats: true) { _ in
            CoinApiService.client.getAllCoinPrices { [weak self] result in
                switch result {
                case .success(let coinPrices):
                    guard let `self` = self else { return }
                    self.allCoinPrices = coinPrices
                    self.coinPrices = coinPrices
                    
                    switch self.selectedFilterType {
                    case .all:
                        self.getAllCoinPrices()
                    case .favorite:
                        self.getFavoriteCoinPrices()
                    }

                case .failure(let error):
                    self?.presenter?.coinPricesFetchFailed(error)
                }
            }
        }
        timer?.fire()
    }
    
    func getFavoriteCoinPrices() {
        let favoriteBaseArray: [String] = UserDefaults.standard.stringArray(forKey: .favoriteCoinPrices)
        self.coinPrices = self.allCoinPrices
        
        let favArray = self.coinPrices.filter { favoriteBaseArray.contains($0.base ?? "") }
        self.coinPrices = favArray
        
        if !self.searchKey.isEmpty {
            self.coinPrices = coinPrices.filter { $0.name?.lowercased().contains(self.searchKey.lowercased()) ?? false }
        }
        
        self.presenter?.coinPricesFetched()
    }
    
    func getAllCoinPrices() {
        self.coinPrices = self.allCoinPrices
        
        if !self.searchKey.isEmpty {
            self.coinPrices = coinPrices.filter { $0.name?.lowercased().contains(self.searchKey.lowercased()) ?? false }
        }
        
        self.presenter?.coinPricesFetched()
    }
    
    func addCoinToFavorites(index: Int) {
        var favoriteBaseArray: [String] = UserDefaults.standard.stringArray(forKey: .favoriteCoinPrices)
        if let coinBase = coinPrices[index].base, favoriteBaseArray.contains(coinBase) == false {
            favoriteBaseArray.append(coinBase)
        }
        UserDefaults.standard.save(favoriteBaseArray, forKey: .favoriteCoinPrices)
    }
    
    func removeCoinFromFavorites(index: Int) {
        var favoriteBaseArray: [String] = UserDefaults.standard.stringArray(forKey: .favoriteCoinPrices)
        if let coinBase = coinPrices[index].base, favoriteBaseArray.contains(coinBase) {
            favoriteBaseArray.removeAll { $0 == coinBase }
        }
        UserDefaults.standard.save(favoriteBaseArray, forKey: .favoriteCoinPrices)
        getFavoriteCoinPrices()
    }
    
    func searchCoinPrices(searchText: String) {
        self.searchKey = searchText
        switch selectedFilterType {
        case .all:
            getAllCoinPrices()
        case .favorite:
            getFavoriteCoinPrices()
        }
    }
    
    func clearSearchCoinPrices() {
        self.searchCoinPrices(searchText: "")
    }
}
