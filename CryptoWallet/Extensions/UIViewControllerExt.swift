//
//  UIViewControllerExt.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 18/06/2021.
//

import Foundation
import UIKit
import SVProgressHUD

extension UIViewController {
    
    func showLoading() {
        SVProgressHUD.setForegroundColor(.systemBlue)
        SVProgressHUD.show()
    }
    
    func hideLoading() {
        SVProgressHUD.dismiss()
    }
}
