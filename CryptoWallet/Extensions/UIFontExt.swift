//
//  UIFontExt.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 18/06/2021.
//

import Foundation
import UIKit

struct PrimaryFontName {
    static let regular = R.font.gilroyRegular.fontName
    static let medium = R.font.gilroyMedium.fontName
    static let semibold = R.font.gilroySemiBold.fontName
}

extension UIFontDescriptor.AttributeName {
    static let nsctFontUIUsage = UIFontDescriptor.AttributeName(rawValue: "NSCTFontUIUsageAttribute")
}


extension UIFont {
    
    @objc class func mediumSystemFont(ofSize pointSize: CGFloat) -> UIFont {
        return self.systemFont(ofSize: pointSize, weight: .medium)
    }
    
    @objc class func semiboldSystemFont(ofSize pointSize: CGFloat) -> UIFont {
        return self.systemFont(ofSize: pointSize, weight: .semibold)
    }
    
    @objc class func mySystemFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: PrimaryFontName.regular, size: size)!
    }

    @objc class func mySemiboldSystemFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: PrimaryFontName.semibold, size: size)!
    }
    
    @objc class func myMediumSystemFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: PrimaryFontName.medium, size: size)!
    }

    @objc convenience init(myCoder aDecoder: NSCoder) {
        guard
            let fontDescriptor = aDecoder.decodeObject(forKey: "UIFontDescriptor") as? UIFontDescriptor,
            let fontAttribute = fontDescriptor.fontAttributes[.nsctFontUIUsage] as? String else {
                self.init(myCoder: aDecoder)
                return
        }
        var fontName = ""
        switch fontAttribute {
        case "CTFontRegularUsage":
            fontName = PrimaryFontName.regular
        case "CTFontEmphasizedUsage", "CTFontBoldUsage", "CTFontSemiboldUsage":
            fontName = PrimaryFontName.semibold
        case "CTFontMediumUsage":
            fontName = PrimaryFontName.medium
        default:
            fontName = PrimaryFontName.regular
        }
        self.init(name: fontName, size: fontDescriptor.pointSize)!
    }

    class func overrideInitialize() {
        guard self == UIFont.self else { return }

        if let systemFontMethod = class_getClassMethod(self, #selector(systemFont(ofSize:))),
            let mySystemFontMethod = class_getClassMethod(self, #selector(mySystemFont(ofSize:))) {
            method_exchangeImplementations(systemFontMethod, mySystemFontMethod)
        }

        if let boldSystemFontMethod = class_getClassMethod(self, #selector(boldSystemFont(ofSize:))),
            let myBoldSystemFontMethod = class_getClassMethod(self, #selector(mySemiboldSystemFont(ofSize:))) {
            method_exchangeImplementations(boldSystemFontMethod, myBoldSystemFontMethod)
        }
        
        if let semiBoldSystemFontMethod = class_getClassMethod(self, #selector(semiboldSystemFont(ofSize:))),
            let mySemiboldSystemFontMethod = class_getClassMethod(self, #selector(mySemiboldSystemFont(ofSize:))) {
            method_exchangeImplementations(semiBoldSystemFontMethod, mySemiboldSystemFontMethod)
        }
        
        if let mediumSystomFont = class_getClassMethod(self, #selector(mediumSystemFont(ofSize:))),
            let myMediumSystomFont = class_getClassMethod(self, #selector(myMediumSystemFont(ofSize:))) {
            method_exchangeImplementations(mediumSystomFont, myMediumSystomFont)
        }

        if let initCoderMethod = class_getInstanceMethod(self, #selector(UIFontDescriptor.init(coder:))), // Trick to get over the lack of UIFont.init(coder:))
            let myInitCoderMethod = class_getInstanceMethod(self, #selector(UIFont.init(myCoder:))) {
            method_exchangeImplementations(initCoderMethod, myInitCoderMethod)
        }
    }
}

