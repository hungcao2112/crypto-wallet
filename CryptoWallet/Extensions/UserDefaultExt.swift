//
//  UserDefaultExt.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 19/06/2021.
//

import Foundation

enum UserDefaultKey: String {
    case favoriteCoinPrices
}

extension UserDefaults {
    
    func save(_ value: Any?, forKey: UserDefaultKey) {
        self.set(value, forKey: forKey.rawValue)
    }
    
    
    func value(forKey: UserDefaultKey) -> Any? {
        self.value(forKey: forKey.rawValue)
    }
    
    func stringArray(forKey: UserDefaultKey) -> [String] {
        self.stringArray(forKey: forKey.rawValue) ?? []
    }
}
