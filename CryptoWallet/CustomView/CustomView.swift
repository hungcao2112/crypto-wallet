//
//  CustomView.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 18/06/2021.
//

import UIKit
import SnapKit

class CustomView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        setupXib()
    }
    
    private func setupXib() {
        guard let xibView = getView() else {
            return
        }
        xibView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(xibView)
        xibView.snp.makeConstraints { (make) in
            make.left.top.bottom.right.equalToSuperview()
        }
    }
    
    func getView() -> UIView? {
        return nil
    }

    deinit {
        print("\(type(of: self)): Deinited")
    }

}
