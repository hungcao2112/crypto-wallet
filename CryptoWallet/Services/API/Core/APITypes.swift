//
//  APITypes.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 16/06/2021.
//

import Foundation
import ObjectMapper

typealias JSON = Any
typealias JSONCompletionHandler = (ApiResult<JSON>) -> Void

// MARK: - Result

enum ApiResult<T> {
    case success(T)
    case failure(ApiError)
}


// MARK: - ApiError

enum ApiError: Error {
    case unauthorized   // 401
    case noConnection   // Timeout | offline
    case badRequest     // 400, 500, ...
    case typeMismatch
    case unknown        // ?
    
    var message: String {
        switch self {
        case .unauthorized:
            return "Your session has been expired. Try again later!"
        case .noConnection:
            return "No connection! Please check your network"
        case .badRequest:
            return "Bad request. Try again later!"
        case .typeMismatch:
            return "Operation failed. Try again later!"
        case .unknown:
            return "Unknown error. Try again later!"
        }
    }
}


// MARK: - Base Model

class APIBaseModel: Mappable {
    
    // MARK: Initializers
    
    init() {}
    
    required init?(map: Map){
        mapping(map: map)
    }
    
    // MARK: Mappable Protocol
    
    func mapping(map: Map) {}
    
    // MARK: - Date Transform
    func dateTransform() -> DateFormatterTransform {
        return self.dateTransform(format: "yyyy-MM-dd'T'HH:mm:ss'Z'")
    }
    
    final func dateTransform(format: String) -> DateFormatterTransform {
        return CustomDateFormatTransform(formatString: format)
    }
}
