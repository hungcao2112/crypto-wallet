//
//  APISerializable.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 16/06/2021.
//

import Foundation
import ObjectMapper

protocol ApiResultSerializable {
    func mapObject<T: Mappable>(_: T.Type, from json: JSON?) -> ApiResult<T>
    func mapObjects<T: Mappable>(_: T.Type, from json: JSON?) -> ApiResult<[T]>
}

extension ApiResultSerializable {
    
    func mapObject<T: Mappable>(_: T.Type, from json: JSON?) -> ApiResult<T> {
        guard let jsonOject = json as? [String: Any],
            let result = Mapper<T>().map(JSONObject: jsonOject["data"]) else {
            return .failure(ApiError.typeMismatch)
        }
        return .success(result)
    }

    func mapObjects<T: Mappable>(_: T.Type, from json: JSON?) -> ApiResult<[T]> {
        guard let jsonOject = json as? [String: Any],
            let result = Mapper<T>().mapArray(JSONObject: jsonOject["data"]) else {
            return .failure(ApiError.typeMismatch)
        }
        return .success(result)
    }
}


