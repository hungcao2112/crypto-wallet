//
//  APIRequest.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 16/06/2021.
//

import Foundation
import Alamofire
import ObjectMapper


// MARK: - Client

class APIRequest {
    private let manager = Alamofire.Session.default
    private let baseURL = App.Config.apiBaseURL + "/v\(App.Config.apiVersion)"
}


// MARK: - Generic JSON Request

extension APIRequest {
    /**
     Create a request using the custom manager for the specified method, URL string, parameters, parameter encoding.
     Response data serialized to JSON
     
     - parameter url: The URL string.
     - parameter method: The HTTP method.
     - parameter parameters: The http request parameters.
     - parameter headers: The http request Headers.
     - parameter encoding: The parameters encoding. `.URL` by default.
     - parameter completionHandler: Completion Handler with JSON object serialized.
     
     - returns: The created request.
     */
    @discardableResult
    func request(endpoint: String, method: Alamofire.HTTPMethod, parameters: [String : Any]? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil, completionHandler: @escaping JSONCompletionHandler) -> Request {
        
        let request = self.manager.request(baseURL + endpoint, method: method, parameters: parameters, encoding: encoding, headers: headers)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
                // Check if request is canceled
                if let code = response.response?.statusCode, code == NSURLErrorCancelled {
                    return
                }
               
                switch response.result {
                case .success:
                    if let data = response.value {
                        completionHandler(ApiResult<JSON>.success(data))
                    } else {
                        completionHandler(ApiResult<JSON>.failure(.typeMismatch))
                    }
                case .failure:
                    let error = self.errorFrom(dataResponse: response)
                    completionHandler(ApiResult<JSON>.failure(error))
                }
        }
        
        return request
    }
}


// MARK: - Serializable

extension APIRequest: ApiResultSerializable {
    
    func requestObject<T: Mappable>(_ type: T.Type, endpoint: String, method: Alamofire.HTTPMethod, parameters: [String : Any]? = nil, headers: HTTPHeaders? = nil, completionHandler: @escaping (ApiResult<T>)->Void) {
        let task = self.request(
            endpoint: endpoint,
            method: method,
            parameters: parameters,
            headers: headers,
            completionHandler: { response in
                switch response {
                case .success(let json):
                    completionHandler(self.mapObject(T.self, from: json))
                case .failure(let error):
                    completionHandler(ApiResult<T>.failure(error))
                }
        })
        task.resume()
    }
    
    func requestObjects<T: Mappable>(_ type: T.Type, endpoint: String, method: Alamofire.HTTPMethod, parameters: [String : Any]? = nil, headers: HTTPHeaders? = nil, completionHandler: @escaping (ApiResult<[T]>)->Void) {
        let task = self.request(
            endpoint: endpoint,
            method: method,
            parameters: parameters,
            headers: headers,
            completionHandler: { response in
                switch response {
                case .success(let json):
                    completionHandler(self.mapObjects(T.self, from: json))
                case .failure(let error):
                    completionHandler(ApiResult<[T]>.failure(error))
                }
        })
        task.resume()
    }
    
}


// MARK: - Error Handler

private extension APIRequest {
    
    private func errorFrom(error: Any?, httpStatusCode: Int?) -> ApiError {
        if let error = error as? NSError, error.code == NSURLErrorTimedOut {
            return .noConnection
        }
        guard let httpStatusCode = httpStatusCode else {
            return ApiError.unknown
        }
        return errorFrom(httpStatuscode: httpStatusCode)
    }
    
    
    private func errorFrom(dataResponse response: AFDataResponse<Any>) -> ApiError {
        return errorFrom(error: response.error, httpStatusCode: response.response?.statusCode)
    }
    
    private func errorFrom(httpStatuscode statusCode: Int) -> ApiError {
        switch(statusCode) {
        case 403, 401:
            return  ApiError.unauthorized
        case 400, 404, 500:
            return ApiError.badRequest
        default:
            return ApiError.unknown
        }
    }
}

