//
//  CoinApiService.swift
//  CryptoWallet
//
//  Created by Cao Thang Hung on 16/06/2021.
//

import Foundation
import ObjectMapper
import Alamofire

class CoinApiService: APIRequest {
    
    public static let client = CoinApiService()
    
    private override init() {
        super.init()
    }
    
    public func getAllCoinPrices(_ completion: @escaping (ApiResult<[Coin]>) -> Void) {
        var params: [String: Any] = [:]
        params["counter_currency"] = "USD"
        self.requestObjects(Coin.self, endpoint: "/price/all_prices_for_mobile", method: .get, completionHandler: completion)
    }
}
