# CryptoWallet

# Cryto Wallet

Hi, this is my app created for Coinhako test. I named my app Cryto Wallet

## Libraries and frameworks used in this app

#### R.swift - https://github.com/mac-cain13/R.swift
- To get strong typed, autocompleted resources like images, fonts and segues in Swift projects

#### Kingfisher - https://github.com/onevcat/Kingfisher
- To easily get images from urls

#### Alamofire - https://github.com/Alamofire/Alamofire
- For HTTP networking use cases

#### ObjectMapper - https://github.com/tristanhimmelman/ObjectMapper
- To convert model objects (classes and structs) to and from JSON.

#### SVProgressHUD - https://github.com/jdg/SVProgressHUD
- To display loading view


Because for my lack of experiences, this app does not contain the UI/Unit testing
Sorry about that :(

Anyway, just enjoy my app. If there is anything I could improve, just tell me

Thank you, and have a nice day!

## ------Pod Install before open project--------
